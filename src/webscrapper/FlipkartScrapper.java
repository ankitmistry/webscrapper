/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webscrapper;

import com.jaunt.Document;
import com.jaunt.Element;
import com.jaunt.NodeNotFound;
import com.jaunt.ResponseException;
import com.jaunt.UserAgent;

/**
 *
 * @author ankit
 */
public class FlipkartScrapper {

    public String title = "<title>";

    public static String getScrap(String html) throws ResponseException, NodeNotFound {
        Document doc = new UserAgent().openContent(html); 
        Element title = doc.findFirst("<title>");
        return title.getText();
    }
}
