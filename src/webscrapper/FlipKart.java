/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webscrapper;

import com.google.gson.stream.JsonReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author ankit
 */
public class FlipKart {

    private String title;
    private String body;
    private String brand;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public static FlipKart scrapFlipkart(String html) throws FileNotFoundException, IOException {

        JsonReader reader = new JsonReader(new FileReader("flipkart.json"));
        reader.beginObject();

        while (reader.hasNext()) {

            String key = reader.nextString();
            System.out.println(key);

        }

        return null;
    }
    
    
}
