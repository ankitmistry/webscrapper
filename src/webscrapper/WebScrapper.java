/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webscrapper;

import com.jaunt.Element;
import com.jaunt.JauntException;
import com.jaunt.UserAgent;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author ankit
 */
public class WebScrapper {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here

        try {
            UserAgent userAgent = new UserAgent();                       //create new userAgent (headless browser).
            userAgent.visit("http://www.flipkart.com/samsung-galaxy-core-i8262/p/itmdhkxywgwhhzk5?pid=MOBDHKWZX5SMWMPX&srno=b_1&ref=ec080af0-2821-481f-b656-b99badab42bb");                        //visit a webpage    
            String html = userAgent.doc.innerHTML();               //print the content as HTML
            
            
            FlipKart fk = new FlipKart();
            Element price = userAgent.doc.findFirst("<span class=\"fk-font-verybig pprice fk-bold\">");
            //System.out.println(price.getText());
            
            FlipKart.scrapFlipkart(html);
            
            //System.out.println(FlipkartScrapper.getTitle(html));
            
        } catch (JauntException e) {         //if an HTTP/connection error occurs, handle JauntException.
            System.err.println(e);
        }


    }
}
